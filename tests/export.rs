mod common;
use common::Environment;

#[test]
fn export() -> anyhow::Result<()> {
    let e = Environment::new()?;

    let (alice, _alice_pgp) = e.gen("alice", None, None);
    let (bob, _bob_pgp) = e.gen("bob", None, None);

    // Add alice.
    e.sq_git(&[
        "policy",
        "authorize",
        "alice", &alice.fingerprint().to_string(),
        "--project-maintainer"
    ])?;
    e.git(&["add", "openpgp-policy.toml"])?;
    e.git(&[
        "commit",
        "-m", "Initial commit.",
        &format!("-S{}", alice.fingerprint()),
    ])?;
    let root = e.git_current_commit()?;

    e.sq_git(&["log", "--trust-root", &root])?;

    e.check_export("alice", None, &[ &alice ]);
    e.check_export(None, None, &[ &alice ]);
    e.check_export("bob", None, &[ ]);

    // Add alice.
    e.sq_git(&[
        "policy",
        "authorize",
        "bob", &bob.fingerprint().to_string(),
        "--project-maintainer"
    ])?;
    e.git(&["add", "openpgp-policy.toml"])?;
    e.git(&[
        "commit",
        "-m", "Add Bob as co-maintainer.",
        &format!("-S{}", alice.fingerprint()),
    ])?;
    let _c2 = e.git_current_commit()?;

    e.sq_git(&["log", "--trust-root", &root])?;

    e.check_export("alice", None, &[ &alice ]);
    e.check_export(None, None, &[ &alice, &bob ]);
    e.check_export("bob", None, &[ &bob ]);

    // Make sure using a commit works.
    e.check_export("alice", &root[..], &[ &alice ]);
    e.check_export(None, &root[..], &[ &alice ]);
    e.check_export("bob", &root[..], &[ ]);

    Ok(())
}
