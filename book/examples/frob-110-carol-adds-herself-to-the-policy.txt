carol$ git switch -c carol/make-carol-a-committer main
Switched to a new branch 'carol/make-carol-a-committer'
carol$ sq-git policy authorize carol --committer \
    --cert CA501F894EBD6193655CB77C476D56394D4E67DC
  - User carol was added.
  - User carol was granted the right sign-commit.
  - User carol: new certificate CA501F894EBD6193655CB77C476D56394D4E67DC.
carol$ git add openpgp-policy.toml
carol$ git commit -m 'Authorize Carol to be a committer.'
[carol/make-carol-a-committer b007000] Authorize Carol to be a committer.
 1 file changed, 36 insertions(+)
carol$ git cherry-pick b006000000000000000000000000000000000000
[carol/make-carol-a-committer b008000] Use an O(log(n)) algorithm instead of one that takes O(n).
 Date: Fri Feb 21 08:42:15 2025 +0100
 1 file changed, 21 insertions(+), 7 deletions(-)
carol$ sq-git log --trust-root 'HEAD^'
b007000000000000000000000000000000000000..b008000000000000000000000000000000000000:
  Signer: carol [CA501F894EBD6193655CB77C476D56394D4E67DC]
  Use an O(log(n)) algorithm instead of one that takes O(n).
Verified that there is an authenticated path from the trust root
b007000000000000000000000000000000000000 to b008000000000000000000000000000000000000.
