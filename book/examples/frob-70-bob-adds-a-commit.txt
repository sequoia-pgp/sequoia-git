bob$ emacs main.rs
bob$ git add main.rs
bob$ git commit -m 'Fix a corner case.'
[main b004000] Fix a corner case.
 1 file changed, 21 insertions(+), 7 deletions(-)
bob$ emacs main.rs
bob$ git add main.rs
bob$ git commit -m 'Add support for ACME'\''s frob.'
[main b005000] Add support for ACME's frob.
 1 file changed, 9 insertions(+), 12 deletions(-)
