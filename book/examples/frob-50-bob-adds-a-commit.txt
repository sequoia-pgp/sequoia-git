bob$ emacs main.rs
bob$ git add main.rs
bob$ git commit -m 'Add a cool new feature.'
[main b003000] Add a cool new feature.
 1 file changed, 21 insertions(+)
 create mode 100644 main.rs
bob$ sq-git log --trust-root 'HEAD^'
b002000000000000000000000000000000000000..b003000000000000000000000000000000000000:
  Signer: bob [B0B50C2B8C3558D225A3310C1A8FAB5E378DD32D]
  Add a cool new feature.
Verified that there is an authenticated path from the trust root
b002000000000000000000000000000000000000 to b003000000000000000000000000000000000000.
