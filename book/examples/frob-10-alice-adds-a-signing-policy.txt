alice$ sq-git policy authorize alice --project-maintainer \
    --cert A11CECD86FB6050466E6259C7993A17BA8537B3D
  - User alice was added.
  - User alice was granted the right sign-commit.
  - User alice was granted the right sign-tag.
  - User alice was granted the right sign-archive.
  - User alice was granted the right add-user.
  - User alice was granted the right retire-user.
  - User alice was granted the right audit.
  - User alice: new certificate A11CECD86FB6050466E6259C7993A17BA8537B3D.
alice$ git add openpgp-policy.toml
alice$ git commit -m 'Add a signing policy.'
[main (root-commit) b001000] Add a signing policy.
 1 file changed, 43 insertions(+)
 create mode 100644 openpgp-policy.toml
