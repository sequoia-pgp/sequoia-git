carol$ git switch -c carol/vroom main
Switched to a new branch 'carol/vroom'
carol$ emacs main.rs
carol$ git add main.rs
carol$ git commit -m 'Use an O(log(n)) algorithm instead of one that takes O(n).'
[carol/vroom b006000] Use an O(log(n)) algorithm instead of one that takes O(n).
 1 file changed, 21 insertions(+), 7 deletions(-)
