# Adding a Signing Policy to a Project

To start using `sq-git` with a project, you first add a signing
policy.  The two most important things that a signing policy says are:
who is allowed to add commits to the project, and who is allowed to
change the policy.

Imagine a project called Frob.  Alice is the project's maintainer, Bob
is a committer, and Carol occasionally makes contributions, but all of
her contributions are first reviewed by Alice or Bob.

As the project's maintainer, Alice is responsible for creating and
maintaining the signing policy.  To mark herself as the project
maintainer, she uses the `sq-git policy authorize` command:

```shell
{{#include ../examples/frob-10-alice-adds-a-signing-policy.txt}}
```

This adds a new entity, `alice`, to the `openpgp-policy.toml` file at
the root of the working tree, gives her all rights, and associates an
OpenPGP certificate with the entity.

By default, `sq-git` reads certificates from your local [shared
OpenPGP certificate directory].  This is the same certificate store
that [`sq`] uses.  By default, it is stored under
`~/.local/share/pgp.cert.d`.  If your tooling doesn't use the shared
OpenPGP certificate directory, you can export the certificate to a
file, and use the `--cert-file` argument instead of the `--cert`
argument to designate the certificate.

  [shared OpenPGP certificate directory]: https://sequoia-pgp.gitlab.io/pgp-cert-d/
  [`sq`]: https://book.sequoia-pgp.org

Before Alice can authorize Bob, she needs to get his certificate.
It's important that she make sure she has the correct certificate, as
otherwise someone else will be allowed to make changes.

Alice might download Bob's certificate from his web page, his profile
page on a forge, a public directory like
[keys.openpgp.org](https://keys.openpgp.org).  This is a good start.
But, before she uses the certificate, she should double check that it
really belongs to Bob by asking him to send her his certificate's
fingerprint via a different communication channel.  The most secure
option would be to meet in real life and exchange fingerprints.  But
sending it via a secure messenger like Signal, or checking the
fingerprint in a call is usually enough.  It's not only important for
Alice to check with Bob for security reasons, though: Bob might want
to use a separate certificate for signing his commits.

Once Alice has verified Bob's certificate, she can authorize him as
follows:

```shell
{{#include ../examples/frob-30-alice-authorizes-bob.txt}}
```

That is, she uses the same command as before, but instead of making
Bob a project maintainer, she makes him a release manager.  This means
that he is allowed to add commits to the project and make releases,
but not modify the project's signing policy.  If Alice doesn't want
Bob to make releases, then she could have made him a committer using
the `--committer` option instead.

As a general rule of thumb, a project should have more than one
project maintainer.  This way, if one maintainer loses access to their
certificate, another person can add a new certificate without
violating the signing policy.

Since modifying the policy is a particularly sensitive operation, you
might consider having two entities with different rights and
certificates: one that is allowed to change the policy, and uses a
certificate that is stored offline, e.g., on an OpenPGP card, and
another that is only allowed to add commits, and uses an online
certificate.
