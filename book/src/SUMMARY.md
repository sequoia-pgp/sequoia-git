# Summary

[Introduction](introduction.md)

- [Signing Commits](signing-commits.md)
- [Adding a Signing Policy to a Project](adding-a-signing-policy.md)
- [Authenticating Commits](authenticating-commits.md)
- [Authenticating a Pull Request](authenticating-a-pull-request.md)
