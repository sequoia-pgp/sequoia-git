# Signing Commits

Signing a commit means adding a digital signature to your commits.
This allows someone who has your certificate to verify that you made
the signature.  Signing commits is a prerequisite to using `sq-git`.

If you are using Sequoia, you can create a certificate as follows:

```shell
$ sq key generate --name "Iliana" --email iliana@example.org --own-key
Please enter the password to protect key (press enter to not use a password):
                                                  Please repeat the password:
 - ┌ 5D4142463C04ADCF7372231DC06A1E7BD5E115DC
   └ Iliana
   - certification created

 - ┌ 5D4142463C04ADCF7372231DC06A1E7BD5E115DC
   └ <iliana@example.org>
   - certification created
...
```

Then you need to configure `git` to use it:

```shell
$ git --global gpg.gpgsign true
$ git --global user.signingKey 5D4142463C04ADCF7372231DC06A1E7BD5E115DC
```
